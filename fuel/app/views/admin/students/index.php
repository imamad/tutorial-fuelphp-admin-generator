<h2>Listing Students</h2>
<br>
<?php if ($students): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Born place</th>
			<th>Born date</th>
			<th>Address</th>
			<th>Age</th>
			<th>School</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($students as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->born_place; ?></td>
			<td><?php echo $item->born_date; ?></td>
			<td><?php echo $item->address; ?></td>
			<td><?php echo $item->age; ?></td>
			<td><?php echo $item->school; ?></td>
			<td>
				<?php echo Html::anchor('admin/students/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/students/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/students/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Students.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/students/create', 'Add new Student', array('class' => 'btn btn-success')); ?>

</p>
