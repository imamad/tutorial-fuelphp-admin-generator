<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Name', 'name', array('class'=>'control-label')); ?>

				<?php echo Form::input('name', Input::post('name', isset($student) ? $student->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Name')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Born place', 'born_place', array('class'=>'control-label')); ?>

				<?php echo Form::input('born_place', Input::post('born_place', isset($student) ? $student->born_place : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Born place')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Born date', 'born_date', array('class'=>'control-label')); ?>

				<?php echo Form::input('born_date', Input::post('born_date', isset($student) ? $student->born_date : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Born date')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Address', 'address', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('address', Input::post('address', isset($student) ? $student->address : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Address')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Age', 'age', array('class'=>'control-label')); ?>

				<?php echo Form::input('age', Input::post('age', isset($student) ? $student->age : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Age')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('School', 'school', array('class'=>'control-label')); ?>

				<?php echo Form::input('school', Input::post('school', isset($student) ? $student->school : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'School')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>