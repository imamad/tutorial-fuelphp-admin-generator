<h2>Viewing #<?php echo $student->id; ?></h2>

<p>
	<strong>Name:</strong>
	<?php echo $student->name; ?></p>
<p>
	<strong>Born place:</strong>
	<?php echo $student->born_place; ?></p>
<p>
	<strong>Born date:</strong>
	<?php echo $student->born_date; ?></p>
<p>
	<strong>Address:</strong>
	<?php echo $student->address; ?></p>
<p>
	<strong>Age:</strong>
	<?php echo $student->age; ?></p>
<p>
	<strong>School:</strong>
	<?php echo $student->school; ?></p>

<?php echo Html::anchor('admin/students/edit/'.$student->id, 'Edit'); ?> |
<?php echo Html::anchor('admin/students', 'Back'); ?>