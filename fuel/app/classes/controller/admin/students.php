<?php
class Controller_Admin_Students extends Controller_Admin{

	public function action_index()
	{
		$data['students'] = Model_Student::find('all');
		$this->template->title = "Students";
		$this->template->content = View::forge('admin/students/index', $data);

	}

	public function action_view($id = null)
	{
		$data['student'] = Model_Student::find($id);

		$this->template->title = "Student";
		$this->template->content = View::forge('admin/students/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Student::validate('create');

			if ($val->run())
			{
				$student = Model_Student::forge(array(
					'name' => Input::post('name'),
					'born_place' => Input::post('born_place'),
					'born_date' => Input::post('born_date'),
					'address' => Input::post('address'),
					'age' => Input::post('age'),
					'school' => Input::post('school'),
				));

				if ($student and $student->save())
				{
					Session::set_flash('success', e('Added student #'.$student->id.'.'));

					Response::redirect('admin/students');
				}

				else
				{
					Session::set_flash('error', e('Could not save student.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Students";
		$this->template->content = View::forge('admin/students/create');

	}

	public function action_edit($id = null)
	{
		$student = Model_Student::find($id);
		$val = Model_Student::validate('edit');

		if ($val->run())
		{
			$student->name = Input::post('name');
			$student->born_place = Input::post('born_place');
			$student->born_date = Input::post('born_date');
			$student->address = Input::post('address');
			$student->age = Input::post('age');
			$student->school = Input::post('school');

			if ($student->save())
			{
				Session::set_flash('success', e('Updated student #' . $id));

				Response::redirect('admin/students');
			}

			else
			{
				Session::set_flash('error', e('Could not update student #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$student->name = $val->validated('name');
				$student->born_place = $val->validated('born_place');
				$student->born_date = $val->validated('born_date');
				$student->address = $val->validated('address');
				$student->age = $val->validated('age');
				$student->school = $val->validated('school');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('student', $student, false);
		}

		$this->template->title = "Students";
		$this->template->content = View::forge('admin/students/edit');

	}

	public function action_delete($id = null)
	{
		if ($student = Model_Student::find($id))
		{
			$student->delete();

			Session::set_flash('success', e('Deleted student #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete student #'.$id));
		}

		Response::redirect('admin/students');

	}


}